import React, { Component } from 'react';
import { View,Button,Text,Image,StyleSheet,TextInput,TouchableOpacity,TouchableHighlight,ImageBackground } from 'react-native';
import { LoginButton, AccessToken,LoginManager } from 'react-native-fbsdk';
import { ifIphoneX } from 'react-native-iphone-x-helper'

export default class Login extends Component {
  constructor(){
    super()
  }

  LoginFacebook = () =>{
    LoginManager.logInWithPermissions(["public_profile"]).then(
      function(result) {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          console.log(
            "Login success with permissions: " +
              result.grantedPermissions.toString()
          );
        }
      },
      function(error) {
        console.log("Login fail with error: " + error);
      }
    );
  }


  initUser(token) {
    // fetch('https://graph.facebook.com/v2.5/me?fields=email,name,friends,picture&access_token=' + token)
    fetch(`https://graph.facebook.com/v2.5/me?fields=email,name,friends,picture&type=normal&access_token=` + token)
    .then((response) => response.json())
    .then((json) => {
      // Some user object has been set up somewhere, build that user here
      // user.name = json.name
      // user.id = json.id
      // user.user_friends = json.friends
      // user.email = json.email
      // user.username = json.name
      // user.loading = false
      // user.loggedIn = true
      // user.avatar = setAvatar(json.id)  
      console.log(json)    
    })
    .catch(() => {
      reject('ERROR GETTING DATA FROM FACEBOOK')
    })
  }
  render() {

    return (
      <View style={styles.containner}>
        <ImageBackground source={require("./assets/images/background.png")} style={styles.ImageBackground}>
          <View style={styles.containner}>
        <View style={{marginTop:20}}>
          <Image
            source={require("./assets/images/logo.png")}
            style={styles.ImageLogo}
          />
          <Text style={styles.HeaderName}>Yiming Mandarin</Text>
        </View>
        <View>
        <TextInput 
           style={styles.TextInput}
           backgroundColor={"#FFF"}
           placeholder="อีเมลล์"
           placeholderTextColor={"#a4a4a5"}
          />
          <TextInput 
           style={styles.TextInput}
           backgroundColor={"#FFF"}
           placeholder="รหัสผ่าน"
           placeholderTextColor={"#a4a4a5"}
          />
          <TouchableOpacity>
            <Text style={{color:"#FFF",fontSize:16,padding:10}}>ลืมรหัสผ่าน?</Text>
          </TouchableOpacity>
          <TouchableHighlight style={{backgroundColor:"#ffcc37",width:280,height:40,borderRadius:100,borderWidth: 1,borderColor: '#fff'}}>
            <Text style={{fontSize:16,padding:10,color:"#261d07",alignSelf:'center'}}>เข้าสู่ระบบ</Text>
          </TouchableHighlight>
          <TouchableOpacity style={{padding:10}}>
          <Text style={{color:"#FFF",fontSize:16,padding:10,alignSelf:'center'}}>สมัครสมาชิก</Text>
          </TouchableOpacity>
          <Text style={{color:"#FFF",fontSize:16,padding:3,alignSelf:'center'}}>หรือ</Text>
        
          <TouchableHighlight style={{backgroundColor:"#276cbb",width:280,height:40,borderRadius:100,borderWidth: 1,borderColor: '#fff',marginTop:10}}>
          <View style={{display:'flex',flexDirection:'row',alignSelf:'center'}}>
          <Image
            source={require("./assets/images/facebook-icon.jpg")}
            style={{width:23,height:23,marginTop:5}}
          />
            <Text style={{fontSize:16,padding:10,color:"#fff",alignSelf:'center',marginTop:-3}}>เข้าสู่ระบบด้วย Facebook</Text>
            </View>
          </TouchableHighlight>
          </View>
        {/* <LoginButton
          onLoginFinished={
            (error, result) => {
              if (error) {
                console.log("login has error: " + result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    this.initUser(data.accessToken.toString())
                  }
                )
              }
            }
          }
          onLogoutFinished={() => console.log("logout.")}/>
          <Button title="Button" onPress={()=>this.LoginFacebook()}><Text>Facebook</Text></Button> */}
          </View>
          </ImageBackground>
      </View>
    );
  }
};
const styles = StyleSheet.create({
  containner:{
    alignItems:'center',
    display:'flex',
    flex:1,
    marginTop:ifIphoneX ? 50 : 20,
  },
  ImageBackground:{
    width: '100%', 
    height: '100%'
  },
  ImageLogo:{
    width:200,
    height:200
  },
  HeaderName:{
    fontSize:25,
    fontWeight:'bold',
    color:"#FFF"
  },
  TextInput:
  {
    width:280,
    padding:10,
    marginTop:40,
    borderRadius:100,
    fontSize:16
  }
})